# Security Policy

## Supported Versions

| Version | Supported Until | [Upstream Supported Until](https://www.qgis.org/en/site/getinvolved/development/roadmap.html#release-schedule) |
| ------- | --------------- | -------------------------------------------------------------------------------------------------------------- |
| <= 3.2  | Unsupported     | Unsupported                                                                                                    |
| 3.4     | 31/06/2022      | Unsupported                                                                                                    |
| 3.6     | Unsupported     | Unsupported                                                                                                    |
| 3.8     | Unsupported     | Unsupported                                                                                                    |
| 3.10    | 31/10/2021      | 19/02/2021                                                                                                     |
| 3.12    | Unsupported     | Unsupported                                                                                                    |
| 3.14    | Unsupported     | Unsupported                                                                                                    |
| 3.16    | 06/01/2023      | 18/02/2022                                                                                                     |
| 3.18    | Unsupported     | Unsupported                                                                                                    |
| 3.20    | 22/10/2021      | 22/10/2021                                                                                                     |
| 3.22    | 22/02/2023      | 22/02/2023                                                                                                     |
| 3.24    | 17/06/2022      | 17/06/2022                                                                                                     |
