# Docker image for QGIS server

## TODO

Ver porque é que a pasta runtime/etc/ssl não está a ser passada par o servidor. Só isso que falta :-)

scp -P 8322 /etc/ssl/certs/ca-certificates.crt root@localhost:/etc/ssl/certs/

Para funcionar com o curl dentro da máquina

curl "https://gisserver.ine.pt/arcgis/services/BASEMAPS/basemap_IGP_STREETS/MapServer/WMSServer?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap&BBOX=-7.553978298535920288,39.64428957416607346,-7.50682803771339735,39.70179173520123328&CRS=CRS:84&WIDTH=625&HEIGHT=761&LAYERS=28&STYLES=&FORMAT=image/png&DPI=96&MAP_RESOLUTION=96&FORMAT_OPTIONS=dpi:96&TRANSPARENT=TRUE" --output x.png

## Build (initial)

```
Cuidado: tirar --no-cache da Makefile

$ DOCKER_TAG=1.6 make build-server
DOCKER_BUILDKIT=1 docker build --target=runner-server --tag=camptocamp/qgis-server:1.1 --build-arg=QGIS_BRANCH=master .

$ docker image ls | grep camptocamp
camptocamp/qgis-server-acceptance          latest               d564e2f34bfe   About a minute ago   1.01GB
camptocamp/qgis-server-acceptance_config   latest               715d10a04f7b   3 minutes ago        11kB
camptocamp/qgis-server                     latest-desktop       2d5fa2f81abf   3 minutes ago        1.54GB
camptocamp/qgis-server                     latest               72250d8e8218   9 minutes ago        1.06GB
camptocamp/postgres                        9.6                  e20cc409d39a   15 months ago        420MB
```

### Test


https://localhost:8380/cgi-bin/qgis_mapserv.fcgi?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetCapabilities&MAP=/etc/qgisserver/project.qgs

```
export QGIS_PROJECT_FILE="postgresql://ine:geo2351sadbot2019@postgres-server:5432?sslmode=disable&dbname=ine&schema=public&project=lugares"
echo $QGIS_PROJECT_FILE

export QUERY_STRING="SERVICE=WMS&VERSION=1.3.0&REQUEST=GetCapabilities"
export QGIS_PROJECT_FILE="/etc/qgisserver/project.qgs"
export QGIS_SERVER_IGNORE_BAD_LAYERS=1
/usr/local/bin/qgis_mapserv.fcgi

export QUERY_STRING="SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap&FORMAT=image%2Fpng&TRANSPARENT=true&tiled=true&STYLES=&LAYERS=ortos_continente&srs=EPSG%3A4326&WIDTH=256&HEIGHT=256&CRS=EPSG%3A4326&BBOX=39.664764404296875%2C-7.675323486328125%2C39.6661376953125%2C-7.6739501953125"

export QGIS_PROJECT_FILE="postgresql://ine:geo2351sadbot2019@postgres-server:5432?sslmode=disable&dbname=ine&schema=public&project=lugares2"
export QGIS_SERVER_IGNORE_BAD_LAYERS=0
export QGIS_DEBUG=9
export PGSERVICEFILE=/etc/qgisserver/pg_service.conf
/usr/local/bin/qgis_mapserv.fcgi


https://gisserver.ine.pt/arcgis/services/BASEMAPS/basemap_IGP_STREETS/MapServer/WMSServer?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap&BBOX=-17.27366538074904057,32.46373555039426151,-16.6030838934954339,33.13239599999999996&CRS=CRS:84&WIDTH=624&HEIGHT=623&LAYERS=12&STYLES=&FORMAT=image/png&DPI=96&MAP_RESOLUTION=96&FORMAT_OPTIONS=dpi:96&TRANSPARENT=TRUE


http://localhost/postgresql/ine/public/lugares/cgi-bin/qgis_mapserv.fcgi?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap&FORMAT=image%2Fpng&TRANSPARENT=true&tiled=true&STYLES=&LAYERS=ortos_continente&srs=EPSG%3A4326&WIDTH=256&HEIGHT=256&CRS=EPSG%3A4326&BBOX=39.664764404296875%2C-7.675323486328125%2C39.6661376953125%2C-7.6739501953125

https://localhost/postgresql/ine/public/lugares/cgi-bin/qgis_mapserv.fcgi?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap&FORMAT=image%2Fpng&TRANSPARENT=true&tiled=true&STYLES=&LAYERS=ortos_continente&srs=EPSG%3A4326&WIDTH=256&HEIGHT=256&CRS=EPSG%3A4326&BBOX=39.664764404296875%2C-7.675323486328125%2C39.6661376953125%2C-7.6739501953125

https://gisserver.ine.pt/arcgis/services/BASEMAPS/basemap_IGP_STREETS/MapServer/WMSServer&crs=CRS:84&dpiMode=7&format=image/png&layers=9&styles&url=https://gisserver.ine.pt/arcgis/services/BASEMAPS/basemap_IGP_STREETS/MapServer/WMSServer

https://gisserver.ine.pt/arcgis/services/BASEMAPS/basemap_IGP_STREETS/MapServer/WMSServer?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap&BBOX=-9.20519453731940196,38.940527000000003,-8.534613050065791739,39.65099354198422077&CRS=CRS:84&WIDTH=625&HEIGHT=662&LAYERS=28&STYLES=&FORMAT=image/png&DPI=96&MAP_RESOLUTION=96&FORMAT_OPTIONS=dpi:96&TRANSPARENT=TRUE




$ docker ps
CONTAINER ID   IMAGE                       COMMAND                  CREATED       STATUS       PORTS                                       NAMES
608787182cde   postgis/postgis:13-master   "docker-entrypoint.s…"   4 hours ago   Up 4 hours   0.0.0.0:5501->5432/tcp, :::5501->5432/tcp   compositionine_postgres-server_1

docker run --env-file .env --publish=8380:443 --publish=8388:80 --volume=/home/jgr/dev/extjs/GeoMasterBoard/docker/composition.ine/etc/qgisserver:/etc/qgisserver --volume=/home/jgr/dev/extjs/GeoMasterBoard/docker/composition.ine/etc/ssl:/etc/ssl camptocamp/qgis-server:1.1
```

```
curl --insecure "https://localhost:8380/postgresql/ine/public/lugares/cgi-bin/qgis_mapserv.fcgi?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetCapabilities"

$ curl "http://localhost:8380/?SERVICE=WMS&REQUEST=GetCapabilities"
<ServerException>Project file error. For OWS services: please provide a SERVICE and a MAP parameter pointing to a valid QGIS project file</ServerException>
```

```
curl "http://localhost:8380/?SERVICE=WMS&REQUEST=GetCapabilities&MAP=postgresql%3A%3Fservice%3Dine%26sslmode%3Ddisable%26dbname%3D%26schema%3Dpublic%26project%3Dlugares"
```

```
curl "http://localhost:8380/postgresql/ine/lugares?SERVICE=WMS&REQUEST=GetCapabilities"

curl "http://localhost:8380/postgresql/ine/public/lugares/cgi-bin/qgis_mapserv.fcgi?SERVICE=WMS&REQUEST=GetCapabilities"

```

curl "http://localhost:8380/?SERVICE=WMS&REQUEST=GetCapabilities&MAP=/etc/qgisserver/project.qgs"

Ver se:
- [ ] file system está bem
- [ ] conteúdos dos ficheiros
- [ ] configuração da rede
- [ ] variáveis de ambiente foram passadas

```
-- perfeito
docker run --rm -it --env-file .env --publish=8380:80 --volume=/home/jgr/dev/extjs/GeoMasterBoard/docker/composition.ine/etc/qgisserver:/etc/qgisserver camptocamp/qgis-server bash

/usr/local/bin/start-server &
```

curl "http://localhost:8380/index.html"
ok!

curl "http://localhost:8380/web/#monitor-lugares"
ok!

curl "http://localhost:8380/cgi-bin/qgis_mapserv.fcgi?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetCapabilities"
ok!

curl "http://localhost:8380/postgresql/ine/public/lugares/cgi-bin/qgis_mapserv.fcgi?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetCapabilities"
ok!
curl "http://localhost:8380/proxy/http://qgis-server/postgresql/ine/public/lugares/cgi-bin/qgis_mapserv.fcgi?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetCapabilities"

curl "http://localhost:8380/postgresql/ine/public/lugares/cgi-bin/qgis_mapserv.fcgi?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap&FORMAT=image%2Fpng&TRANSPARENT=true&tiled=true&STYLES=&LAYERS=ortos_continente&srs=EPSG%3A4326&WIDTH=256&HEIGHT=256&CRS=EPSG%3A4326&BBOX=40.078125%2C-8.4375%2C40.4296875%2C-8.0859375" --output x.png
ok!

curl "http://localhost:8380/postgresql/ine/public/lugares/cgi-bin/qgis_mapserv.fcgi?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap&FORMAT=image%2Fpng&TRANSPARENT=true&STYLES=&LAYERS=freguesia&CRS=EPSG%3A4326&WIDTH=1117&HEIGHT=840&BBOX=39.4232177734375%2C-8.766983032226562%2C40.5767822265625%2C-7.2330169677734375"

curl "http://localhost:8380/proxy/http://qgis-server/postgresql/ine/public/lugares/cgi-bin/qgis_mapserv.fcgi?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap&FORMAT=image%2Fpng&TRANSPARENT=true&STYLES=&LAYERS=monitor&CRS=EPSG%3A4326&WIDTH=1345&HEIGHT=840&BBOX=39.27473179742938%2C-9.469524121264783%2C39.85151402399188%2C-8.54598591325697" --output x.png

curl "http://localhost:8380/proxy/http://qgis-server/postgresql/ine/public/lugares/cgi-bin/qgis_mapserv.fcgi?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap&FORMAT=image%2Fpng&TRANSPARENT=true&STYLES=&LAYERS=monitor&CRS=EPSG%3A4326&WIDTH=1345&HEIGHT=840&BBOX=39.27473179742938%2C-9.469524121264783%2C39.85151402399188%2C-8.54598591325697" --output x.png


https://cartografia.dgterritorio.gov.pt/ortos2018/service?layer=Ortos2018-RGB&style=&tilematrixset=PTTM_06&Service=WMTS&Request=GetTile&Version=1.0.0&Format=image%2Fpng&TileMatrix=08&TileCol=53&TileRow=68



curl "http://localhost:8380/?MAP=/etc/qgisserver/project.qgs&SERVICE=WMS&VERSION=1.3.0&REQUEST=GetLegendGraphic&LAYER=raa_central_e_oriental_freguesia&FORMAT=image/png&STYLE=default&SLD_VERSION=1.1.0"  --output x.png

curl "http://localhost:8380/?MAP=/etc/qgisserver/project.qgs&SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap&BBOX=-8.351362554191616994,40.3259914496557812,-7.873520500000000588,41.08710514076339138&CRS=CRS:84&WIDTH=501&HEIGHT=799&LAYERS=cont_freguesia&STYLES=&FORMAT=image/png&DPI=96&MAP_RESOLUTION=96&FORMAT_OPTIONS=dpi:96&TRANSPARENT=TRUE" --output z.png

curl "http://localhost:8380/?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap&BBOX=-8.351362554191616994,40.3259914496557812,-7.873520500000000588,41.08710514076339138&CRS=CRS:84&WIDTH=501&HEIGHT=799&LAYERS=cont_freguesia&STYLES=&FORMAT=image/png&DPI=96&MAP_RESOLUTION=96&FORMAT_OPTIONS=dpi:96&TRANSPARENT=TRUE" --output z.png


### ENV

Se for passado QGIS_PROJECT_FILE, o MAP indicado noURL não é usado. É sempre usado o projeto apontado por QGIS_PROJECT_FILE.

Se não for passado QGIS_PROJECT_FILE, tem que se passar um MAP nos pedidos.

## Build

Configure files to be copied to server:

- [ ] `runtime/etc/qgisserver/pg_service.conf`
- [ ] 

```
make build-server
make build-server QGIS_BRANCH=release-3_24 DOCKER_BASE=geomaster/qgis-server DOCKER_TAG=latest make
```


## Usage

Expects a `project.qgs` project file and all the files it depends on in the `/etc/qgisserver/`
directory. Either you create another image to add those files or you inject them using
a volume. For example:

```bash
docker run --detach --publish=8380:80 --volume=$PWD/etc/qgisserver:/etc/qgisserver camptocamp/qgis-server
```

With the previous command, you'll get to your server with this URL:
http://localhost:8380/?SERVICE=WMS&REQUEST=GetCapabilities

## Tuning

You can use the following variables (`-e` option in `docker run`):

- `QGIS_CATCH_SEGV`: Set to `1` if you want stacktraces in the logs in case of segmentation faults.
- `FCGID_MAX_REQUESTS_PER_PROCESS`: The number of requests a QGIS server will serve before being restarted by apache
- `FCGID_MIN_PROCESSES`: The minimum number of fcgi processes to keep (defaults to `1`)
- `FCGID_MAX_PROCESSES`: The maximum number of fcgi processes to keep (defaults to `5`)
- `FCGID_IO_TIMEOUT`: This is the maximum period of time the module will wait while trying to read from or
  write to a FastCGI application (default is `40`)
- `FCGID_BUSY_TIMEOUT`: The maximum time limit for request handling (defaults to `300`)
- `FCGID_IDLE_TIMEOUT`: Application processes which have not handled a request for
  this period of time will be terminated (defaults to `300`)
- `FILTER_ENV`: Filter the environment variables with e.g.:
  `| grep -vi _SERVICE_ | grep -vi _TCP | grep -vi _UDP | grep -vi _PORT` to remove the default
  Kubernetes environment variables (default in an empty string)
- `GET_ENV`: alternative to `FILTER_ENV`, a command that return the environment variables (defaults to `env`)

[See also QGIS server documentation](https://docs.qgis.org/latest/en/docs/server_manual/config.html?highlight=environment#environment-variables)

Fonts present in the `/etc/qgisserver/fonts` directory will be installed and thus usable by QGIS.

## Running the client

If you want to edit a project file, you can run the client from a Linux machine with the following command:

```bash
docker run --rm -ti --env=DISPLAY=unix${DISPLAY} --volume=/tmp/.X11-unix:/tmp/.X11-unix --volume=${HOME}:${HOME} camptocamp/qgis-server:master-desktop
```

## Changelog

### QGIS 3.22

We removed the default values for the following environment variables to better fit with the QGIS documentation:

- `QGIS_SERVER_LOG_LEVEL`, was `0`
- `QGIS_PROJECT_FILE`, was `/etc/qgisserver/project.qgs`
- `MAX_CACHE_LAYERS`, was `""`
- `QGIS_AUTH_DB_DIR_PATH`, was `/etc/qgisserver/`
- `PGSERVICEFILE`, was `/etc/qgisserver/pg_service.conf`
